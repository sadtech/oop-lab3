package ru.rsreu.oop_lab3.task1.parser;

import javafx.collections.ObservableList;

import java.util.Set;

public interface Parser {

    void parse();

    ObservableList<String> getParse();

    void setText(String text);

    Set<String> getParseSet();

}
