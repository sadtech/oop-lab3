package ru.rsreu.oop_lab3.task1.save;


import javafx.scene.control.ListView;

import java.io.FileWriter;
import java.io.IOException;

public class SaveFile implements Save {

    private ListView listView;
    private String savePath;

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    @Override
    public void save() {
        try (FileWriter writer = new FileWriter(savePath, false)) {
            String text = listView.getItems().toString();
            writer.write(text);
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
