package ru.rsreu.oop_lab3.task1;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rsreu.oop_lab3.task1.config.SpringConfig;

public class Main extends Application {

    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    @Override
    public void start(Stage stage) {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        stage = context.getBean(Stage.class);
        stage.show();
    }

    public void run() {
        launch();
    }
}
