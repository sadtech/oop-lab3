package ru.rsreu.oop_lab3.task1.parser;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserTextField implements Parser {

    private String text;
    private Set<String> parseSet;

    public ParserTextField() {
        parseSet = new HashSet<>();
    }

    public ObservableList<String> getParse() {
        ObservableList<String> col = FXCollections.observableArrayList(parseSet);
        return col;
    }

    public Set<String> getParseSet() {
        return parseSet;
    }

    public void parse() {
        Pattern p = Pattern.compile("[а-яА-Я0-9]+");
        Matcher m = p.matcher(text);
        while (m.find()) {
            parseSet.add(m.group());
        }
    }

    public void setText(String text) {
        this.text = text;
    }
}
