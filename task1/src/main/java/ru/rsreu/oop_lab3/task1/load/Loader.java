package ru.rsreu.oop_lab3.task1.load;

import javafx.scene.control.ListView;
import ru.rsreu.oop_lab3.task1.parser.Parser;
import ru.rsreu.oop_lab3.task1.parser.ParserTextField;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Loader implements Load {

    private ListView listView;
    private String pathLoad;
    private String text;
    private Parser parser;

    public Loader() {
        parser = new ParserTextField();
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void setPathLoad(String pathLoad) {
        this.pathLoad = pathLoad;
    }

    private void read() {
        try (FileReader reader = new FileReader(this.pathLoad)) {
            char[] buf = new char[256];
            text = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                text += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String getPathLoad() {
        return pathLoad;
    }

    @Override
    public void load() {
        text = "";
        read();
        parser.getParseSet().clear();
        parser.getParse().clear();
        parser.setText(text);
        parser.parse();
        listView.setItems(parser.getParse());
    }
}
