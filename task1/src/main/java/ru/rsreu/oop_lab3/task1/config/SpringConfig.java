package ru.rsreu.oop_lab3.task1.config;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rsreu.oop_lab3.task1.Main;
import ru.rsreu.oop_lab3.task1.controler.GeneralControler;

import java.io.IOException;

@Configuration
public class SpringConfig {

    @Bean
    public Stage stage() {
        Stage stage = new Stage();
        stage.setScene(scene());
        stage.setTitle("Laba3");
        stage.setOnCloseRequest(generalControler().getCloseEventHandler());
        return stage;
    }

    @Bean
    public Scene scene() {
        return new Scene(parent());
    }

    @Bean
    public FXMLLoader fxmlLoader() {
        FXMLLoader loader = new FXMLLoader();
        return loader;
    }

    @Bean
    public Parent parent() {
        String fxmlFile = "/fxml/window.fxml";
        Parent parent = null;
        try {
            parent = fxmlLoader().load(Main.class.getResourceAsStream(fxmlFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    @Bean
    public GeneralControler generalControler() {
        return fxmlLoader().getController();
    }

}
