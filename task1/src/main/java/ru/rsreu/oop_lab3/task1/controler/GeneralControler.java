package ru.rsreu.oop_lab3.task1.controler;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.WindowEvent;
import ru.rsreu.oop_lab3.task1.load.Loader;
import ru.rsreu.oop_lab3.task1.parser.Parser;
import ru.rsreu.oop_lab3.task1.parser.ParserTextField;
import ru.rsreu.oop_lab3.task1.save.Save;
import ru.rsreu.oop_lab3.task1.save.SaveFile;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class GeneralControler implements Initializable {

    @FXML
    private TabPane tabPane;

    @FXML
    private TextField task1_textfield_mes;

    private Parser parser;
    private List<String> oldItem;

    public void initialize(URL location, ResourceBundle resources) {
        parser = new ParserTextField();
        if (alertYesNo("Загрузка", "Загрузить данные?", Alert.AlertType.INFORMATION)) {
            loadItems();
        }
        oldItem = getItemListView();
    }

    private List<String> getItemListView() {
        List<String> stringList = new ArrayList<>();
        for (Tab tab : tabPane.getTabs()) {
            AnchorPane anchorPane = (AnchorPane) tab.getContent();
            ListView listView = (ListView) anchorPane.getChildren().get(0);
            if (listView.getItems().size() > 0) {
                stringList.add(listView.getItems().toString());
            }
        }
        return stringList;
    }

    public boolean checkEdit() {
        List<String> newItem = getItemListView();
        return !newItem.equals(oldItem);
    }

    public void loadItems() {
        String path = foldSelect();
        Loader load = new Loader();
        for (Tab tab : tabPane.getTabs()) {
            AnchorPane anchorPane = (AnchorPane) tab.getContent();
            ListView listView = (ListView) anchorPane.getChildren().get(0);
            load.setPathLoad(path + "/" + tab.getId() + ".txt");
            load.setListView(listView);
            load.load();
        }
    }

    public void addRecord() {
        AnchorPane anchorPane = (AnchorPane) checkTabActive().getContent();
        ListView listView = (ListView) anchorPane.getChildren().get(0);
        parser.getParseSet().clear();
        parser.getParseSet().addAll(listView.getItems());
        parser.setText(task1_textfield_mes.getText());
        parser.parse();
        listView.setItems(parser.getParse());
        task1_textfield_mes.setText("");
    }

    private Tab checkTabActive() {
        for (Tab tab : tabPane.getTabs()) {
            if (tab.isSelected()) {
                return tab;
            }
        }
        return null;
    }

    public void delItem() {
        AnchorPane anchorPane = (AnchorPane) checkTabActive().getContent();
        ListView listView = (ListView) anchorPane.getChildren().get(0);
        if (listView.getItems().size() > 0) {
            if (listView.getSelectionModel().getSelectedItems().size() > 0) {
                int pos = listView.getSelectionModel().getSelectedIndex();
                listView.getItems().remove(pos);
                if (listView.getItems().size() > 0) {
                    System.out.println(pos);
                    System.out.println(listView.getItems().size());
                    if (pos == listView.getItems().size()) {
                        pos -= 1;
                    }
                    task1_textfield_mes.setText(listView.getItems().get(pos).toString());
                }
            } else {
                alert("Выберете элемент для удаления");
            }
        } else {
            alert("Удалять нечего :)");
        }
    }

    private boolean checkItem(String item, ListView listView) {
        for (Object string : listView.getItems()) {
            if (string.equals(task1_textfield_mes.getText())) {
                return true;
            }
        }
        return false;
    }

    public void editItem() {
        AnchorPane anchorPane = (AnchorPane) checkTabActive().getContent();
        ListView listView = (ListView) anchorPane.getChildren().get(0);
        if (listView.getItems().size() > 0) {
            if (listView.getSelectionModel().getSelectedItems().size() > 0) {
                if (!checkItem(task1_textfield_mes.getText(), listView)) {
                    listView.getItems().set(listView.getSelectionModel().getSelectedIndex(), task1_textfield_mes.getText());
                } else {
                    alert("Такой элемент уже есть!");
                }
            } else {
                alert("Выберете элемент для изменения");
            }
        } else {
            alert("Изменять нечего");
        }
        task1_textfield_mes.setText("");
    }

    public void clickList() {
        AnchorPane anchorPane = (AnchorPane) checkTabActive().getContent();
        ListView listView = (ListView) anchorPane.getChildren().get(0);
        if (listView.getItems().size() > 0) {
            task1_textfield_mes.setText(String.valueOf(listView.getSelectionModel().getSelectedItems().get(0)));
        }
    }

    private void alert(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    private boolean alertYesNo(String title, String text, Alert.AlertType alertType) {
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(alertType,
                text, yes, no);
        alert.setTitle(title);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.orElse(no) == yes) {
            return true;
        } else {
            return false;
        }
    }

    public void cleanAllItems() {
        AnchorPane anchorPane = (AnchorPane) checkTabActive().getContent();
        ListView listView = (ListView) anchorPane.getChildren().get(0);
        listView.getItems().clear();
    }

    public void saveItem(String path) {
        Save save = new SaveFile();
        for (Tab tab : tabPane.getTabs()) {
            AnchorPane anchorPane = (AnchorPane) tab.getContent();
            ListView listView = (ListView) anchorPane.getChildren().get(0);
            if (listView.getItems().size() > 0) {
                ((SaveFile) save).setSavePath(path + "/" + tab.getId() + ".txt");
                ((SaveFile) save).setListView(listView);
                save.save();
            }
        }
    }

    public void closeApp() {
        if (checkEdit()) {
            if (alertYesNo("Сохранени", "Информация не сохранена, сохранить?", Alert.AlertType.WARNING)) {
                saveItem(foldSelect());
            }
        }
        System.exit(0);
    }

    private String foldSelect() {
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Выберете файл");
        fileChooser.setInitialDirectory(new File("/Users/upagge/IdeaProjects/Rsreu/oop-lab3/task1"));
        File dir = fileChooser.showDialog(null);
        if (dir != null) {
            return dir.getAbsolutePath();
        } else {
            return null;
        }
    }

    private javafx.event.EventHandler<WindowEvent> closeEventHandler = event -> closeApp();

    public javafx.event.EventHandler<WindowEvent> getCloseEventHandler() {
        return closeEventHandler;
    }

}
