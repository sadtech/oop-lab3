package ru.rsreu.oop_lab3.task2.parse;

import java.util.List;

public interface Parse {

    void parse();

    List<String> getWords();

}
