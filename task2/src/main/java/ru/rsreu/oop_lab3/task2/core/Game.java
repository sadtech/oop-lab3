package ru.rsreu.oop_lab3.task2.core;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game {

    private WordService words;
    private Integer numberOfAttempts;

    public Game(WordService words) {
        this.words = words;
        numberOfAttempts = 0;
    }

    public void incNumberOfAttempts() {
        numberOfAttempts++;
    }

    public void newGame() {
        numberOfAttempts = 0;
        wordСhoice();
        initWord();
    }

    private void wordСhoice() {
        Random random = new Random();
        int numberChoice = random.nextInt(words.getWords().size());
        String tempWord = words.getWords().get(numberChoice);
        List<Symbol> word = new ArrayList<>();
        for (char symbol : tempWord.toCharArray()) {
            word.add(new Symbol(String.valueOf(symbol), false));
        }
        words.setWord(word);
    }

    private void initWord() {
        List<Symbol> word = words.getWord();
        String tempA = word.get(0).getValue();
        String tempZ = word.get(words.getWord().size() - 1).getValue();
        for (Symbol symbol : word) {
            if (symbol.getValue().equals(tempA) || symbol.getValue().equals(tempZ)) {
                symbol.setCheck(true);
            }
        }
    }

    public String printWord() {
        StringBuilder stringBuffer = new StringBuilder();
        for (Symbol symbol : words.getWord()) {
            if (symbol.isCheck()) {
                stringBuffer.append(symbol.getValue());
            } else {
                stringBuffer.append("*");
            }
        }
        return stringBuffer.toString();
    }

    public boolean checkSymbol(String symbol) {
        for (Symbol symbolWord : words.getWord()) {
            if (symbol.equals(symbolWord.getValue()) && !symbolWord.isCheck()) {
                return true;
            }
        }
        return false;
    }

    public void settingSymbol(String symbol) {
        for (Symbol symbolWord : words.getWord()) {
            if (symbol.equals(symbolWord.getValue()) && !symbolWord.isCheck()) {
                symbolWord.setCheck(true);
            }
        }
    }

    public List<String> falseSymbol() {
        List<String> stringList = new ArrayList<>();
        for (Symbol symbol : getSymbolWord()) {
            stringList.add(symbol.getValue());
        }
        for (int i = stringList.size(); i < 42; i++) {
            stringList.add(String.valueOf((char) (1072 + (int) (Math.random() * ((1103 - 1072) + 1)))));
        }
        Collections.shuffle(stringList);
        return stringList;
    }

    private List<Symbol> getSymbolWord() {
        return words.getWord();
    }

    public boolean endGame() {
        for (Symbol symbol : words.getWord()) {
            if (!symbol.isCheck()) {
                return false;
            }
        }
        return true;
    }

    public Integer getNumberOfAttempts() {
        return numberOfAttempts;
    }

    public boolean gameOver() {
        return numberOfAttempts >= 9;
    }
}
