package ru.rsreu.oop_lab3.task2.reader;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReaderFile implements Reader {

    private String path;
    private String text;

    public ReaderFile(String path) {
        this.path = path;
    }

    public String getText() {
        return text;
    }

    public void setPatch(String patch) {
        this.path = patch;
    }

    public void read() {
        try (FileReader reader = new FileReader(path)) {
            char[] buf = new char[256];
            text = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                text += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
