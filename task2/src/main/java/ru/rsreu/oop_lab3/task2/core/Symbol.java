package ru.rsreu.oop_lab3.task2.core;

public class Symbol {

    private String value;
    private boolean check;

    public Symbol(String value, boolean check) {
        this.value = value;
        this.check = check;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
