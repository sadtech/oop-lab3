package ru.rsreu.oop_lab3.task2.core;

import java.util.List;

public interface WordService {

    List<String> getWords();

    void setWord(List<Symbol> word);

    List<Symbol> getWord();

}
