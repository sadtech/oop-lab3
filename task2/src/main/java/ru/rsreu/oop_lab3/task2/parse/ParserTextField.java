package ru.rsreu.oop_lab3.task2.parse;

import ru.rsreu.oop_lab3.task2.reader.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserTextField implements Parse {

    private String text;
    private List<String> parseSet;
    private Reader reader;

    public ParserTextField(Reader reader) {
        this.reader = reader;
        parseSet = new ArrayList<>();
    }

    public void parse() {
        reader.read();
        text = reader.getText();
        Pattern p = Pattern.compile("[а-яА-Я]+");
        Matcher m = p.matcher(text);
        while (m.find()) {
            parseSet.add(m.group());
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getWords() {
        return parseSet;
    }
}
