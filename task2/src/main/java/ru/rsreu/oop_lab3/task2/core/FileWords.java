package ru.rsreu.oop_lab3.task2.core;

import ru.rsreu.oop_lab3.task2.parse.Parse;

import java.util.List;

public class FileWords implements WordService {

    private List<String> words;
    private List<Symbol> word;

    private Parse parse;

    public FileWords(Parse parse) {
        this.parse = parse;
        parse.parse();
        words = parse.getWords();
    }

    public List<String> getWords() {
        return words;
    }

    public List<Symbol> getWord() {
        return word;
    }

    public void setWord(List<Symbol> word) {
        this.word = word;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
}
