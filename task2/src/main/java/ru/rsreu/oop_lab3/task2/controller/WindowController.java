package ru.rsreu.oop_lab3.task2.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.rsreu.oop_lab3.task2.core.Game;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class WindowController implements Initializable {

    private Game game;

    @FXML
    private FlowPane flowPane;

    @FXML
    private Canvas canvas;

    private GraphicsContext gc;

    @FXML
    private Label word;

    public void initialize(URL location, ResourceBundle resources) {
        ApplicationContext context = new ClassPathXmlApplicationContext("xml/context.xml");
        game = (Game) context.getBean("game");
        game.newGame();
        word.setText(game.printWord());
        addButton(game.falseSymbol());
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
    }

    private void print() {
        switch (game.getNumberOfAttempts()) {
            case 1:
                gc.fillRect(75, 75, 2, 140);
                break;
            case 2:
                gc.fillRect(75, 75, 70, 2);
                break;
            case 3:
                gc.fillRect(120, 75, 2, 35);
                break;
            case 4:
                gc.fillOval(112, 110, 20, 25);
                break;
            case 5:
                gc.fillOval(112, 135, 20, 42);
                break;
            case 6:
                gc.beginPath();
                gc.lineTo(150, 120);
                gc.lineTo(125, 170);
                gc.closePath();
                gc.stroke();
                break;
            case 7:
                gc.beginPath();
                gc.lineTo(115, 160);
                gc.lineTo(90, 120);
                gc.closePath();
                gc.stroke();
                break;
            case 8:
                gc.beginPath();
                gc.lineTo(127, 170);
                gc.lineTo(127, 210);
                gc.closePath();
                gc.stroke();
                break;
            case 9:
                gc.beginPath();
                gc.lineTo(118, 170);
                gc.lineTo(118, 210);
                gc.closePath();
                gc.stroke();
                break;
        }
    }


    public void clickButSymbol(ActionEvent event) {
        StringBuffer stringBuffer = new StringBuffer();
        Button button = (Button) event.getSource();
        String userSymbol = button.getText();
        if (game.checkSymbol(userSymbol)) {
            game.settingSymbol(userSymbol);
            word.setText(game.printWord());
            stringBuffer.append("-fx-background-color: green; ");
        } else {
            game.incNumberOfAttempts();
            print();
            stringBuffer.append("-fx-background-color: red; ");
        }
        stringBuffer.append("-fx-text-fill: white; ");
        button.setStyle(stringBuffer.toString());
        button.setDisable(true);

        if (game.gameOver()) {
            word.setText("GameOver");

            for (Node but : flowPane.getChildren()) {
                Button button1 = (Button) but;
                button1.setDisable(true);
            }

        } else if (game.endGame()) {
            newGame();
        }
    }

    private void addButton(List<String> setSymbol) {
        for (String symbol : setSymbol) {
            Button button = new Button(symbol);
            button.setMinSize(29, 29);
            button.setMaxSize(29, 29);
            button.setOnAction(e -> clickButSymbol(e));
            flowPane.getChildren().add(button);
            flowPane.setVgap(3);
            flowPane.setHgap(3);
        }
    }

    public void newGame() {
        flowPane.getChildren().clear();
        game.newGame();
        addButton(game.falseSymbol());
        word.setText(game.printWord());
        gc.clearRect(0,0,300,300);
    }
}
