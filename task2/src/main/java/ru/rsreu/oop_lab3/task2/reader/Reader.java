package ru.rsreu.oop_lab3.task2.reader;

public interface Reader {

    void read();

    String getText();
}
